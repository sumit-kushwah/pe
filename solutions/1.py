sumthree = 0
sumfive = 0
sumfifteen = 0

start = 3

while start < 1000:
  sumthree += start
  start += 3

start = 5

while start < 1000:
  sumfive += start
  start += 5

start = 15

while start < 1000:
  sumfifteen += start
  start += 15

ans = sumthree + sumfive - sumfifteen

print(ans)